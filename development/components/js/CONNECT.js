// ============================
//    Name: index.js
// ============================

import AddMoveScrollUP from './modules/moveScrollUP';
import AddFetchServerData from './modules/fetchServerData';
import AddPhoneInputMask from './modules/phoneInputMask';

const start = () => {
	console.log('DOM:', 'DOMContentLoaded', true);

	new AddMoveScrollUP('.js__moveScrollUP').run();
	
	new AddPhoneInputMask({
		selector: '[data-phone="true"]',
		inputid: '#form-phone'
	}).run();
	
	new AddFetchServerData('.js__galleryData', {
		server: 'my-server.json',
		selector: 'data-gallery-init'
	}).run();
	
};

if (typeof window !== 'undefined' && window && window.addEventListener) {
	document.addEventListener('DOMContentLoaded', start(), false);
}
